# Patrick Kox <patrick.kox@proximus.be>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2017-04-01 09:00+0900\n"
"PO-Revision-Date: 2017-04-28 10:36+0200\n"
"Last-Translator: Patrick Kox <patrick.kox@proximus.be>\n"
"Language-Team: Dutch; Flemish <Debian Administrator Handbook Translators Maillist <debian-handbook-translators@lists.alioth.debian.org>>\n"
"Language: nl-NL\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Gtranslator 2.91.7\n"

msgid "Raphaël"
msgstr "Raphaël"

msgid "Hertzog"
msgstr "Hertzog"

msgid "Roland"
msgstr "Roland"

msgid "Mas"
msgstr "Mas"
