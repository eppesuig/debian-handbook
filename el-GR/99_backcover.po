msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2022-07-30 18:23+0200\n"
"PO-Revision-Date: 2019-12-05 11:05+0000\n"
"Last-Translator: Vangelis Skarmoutsos <skarmoutsosv@gmail.com>\n"
"Language-Team: Greek <https://hosted.weblate.org/projects/debian-handbook/99_backcover/el/>\n"
"Language: el-GR\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.10-dev\n"

msgid "The Debian Administrator's Handbook"
msgstr "Εγχειρίδιο διαχειριστή Debian"

#, fuzzy
#| msgid "Debian Jessie from Discovery to Mastery"
msgid "Debian Bullseye from Discovery to Mastery"
msgstr "Το Debian Jessie από την ανακάλυψη ως την άρτια εκμάθηση"

msgid "Debian GNU/Linux, a very popular non-commercial Linux distribution, is known for its reliability and richness. Built and maintained by an impressive network of thousands of developers throughout the world, the Debian project is cemented by its social contract. This foundation text defines the project's objective: fulfilling the needs of users with a 100% free operating system. The success of Debian and of its ecosystem of derivative distributions (with Ubuntu at the forefront) means that an increasing number of administrators are exposed to Debian's technologies."
msgstr "Το Debian GNU / Linux, μια πολύ δημοφιλής μη εμπορική διανομή Linux, είναι γνωστή για την αξιοπιστία και τον πλούτο του. Χτισμένο και συντηρημένο από ένα εντυπωσιακό δίκτυο χιλιάδων προγραμματιστών σε όλο τον κόσμο, το έργο Debian εδραιώνεται με το κοινωνικό του συμβόλαιο. Αυτό το ιδρυτικό κείμενο ορίζει τον στόχο του έργου: εκπλήρωση των αναγκών των χρηστών με 100% ελεύθερο λειτουργικό σύστημα. Η επιτυχία του Debian και του οικοσυστήματος παράγωγων διανομών (με το Ubuntu στο προσκήνιο) σημαίνει ότι όλο και περισσότεροι διαχειριστές εκτίθενται στις τεχνολογίες του Debian."

#, fuzzy
#| msgid "This Debian Administrator's Handbook, which has been entirely updated for Debian 8 “Jessie”, builds on the success of its 6 previous editions. Accessible to all, this book teaches the essentials to anyone who wants to become an effective and independent Debian GNU/Linux administrator. It covers all the topics that a competent Linux administrator should master, from installation to updating the system, creating packages and compiling the kernel, but also monitoring, backup and migration, without forgetting advanced topics such as setting up SELinux or AppArmor to secure services, automated installations, or virtualization with Xen, KVM or LXC."
msgid "This Debian Administrator's Handbook, which has been entirely updated for Debian 11 “Bullseye”, builds on the success of its 9 previous editions. Accessible to all, this book teaches the essentials to anyone who wants to become an effective and independent Debian GNU/Linux administrator. It covers all the topics that a competent Linux administrator should master, from installation to updating the system, creating packages and compiling the kernel, but also monitoring, backup and migration, without forgetting advanced topics such as setting up SELinux or AppArmor to secure services, automated installations, or virtualization with Xen, KVM or LXC."
msgstr "Αυτό το Εγχειρίδιο Διαχειριστή Debian, το οποίο έχει ενημερωθεί πλήρως για το Debian 8 \"Jessie\", βασίζεται στην επιτυχία των 6 προηγούμενων εκδόσεων του. Προσβάσιμο σε όλους, αυτό το βιβλίο διδάσκει τα βασικά στοιχεία σε όποιον θέλει να γίνει ένας αποτελεσματικός και ανεξάρτητος διαχειριστής του Debian GNU/Linux. Καλύπτει όλα τα θέματα που πρέπει να ελέγξει ένας αρμόδιος διαχειριστής Linux, από την εγκατάσταση έως την ενημέρωση του συστήματος, τη δημιουργία πακέτων και την κατάρτιση του πυρήνα, αλλά και την παρακολούθηση, τη δημιουργία αντιγράφων ασφαλείας και τη ενσωμάτωση, χωρίς να ξεχνάμε τα προηγμένα θέματα όπως η εγκατάσταση SELinux ή AppArmor σε ασφαλείς υπηρεσίες, αυτοματοποιημένες εγκαταστάσεις ή virtualization με Xen, KVM ή LXC."

msgid "This book is not only designed for professional system administrators. Anyone who uses Debian or Ubuntu on their own computer is de facto an administrator and will find tremendous value in knowing more about how their system works. Being able to understand and resolve problems will save you invaluable time."
msgstr "Το βιβλίο αυτό δεν έχει σχεδιαστεί μόνο για επαγγελματίες διαχειριστές συστημάτων. Όποιος χρησιμοποιεί το Debian ή το Ubuntu στον υπολογιστή του είναι de facto διαχειριστής και θα βρει τεράστια αξία γνωρίζοντας περισσότερα για το πώς λειτουργεί το σύστημά του. Η ικανότητα κατανόησης και επίλυσης προβλημάτων θα σας εξοικονομήσει πολύτιμο χρόνο."

msgid "<emphasis role=\"strong\">Raphaël Hertzog</emphasis> is a computer science engineer who graduated from the National Institute of Applied Sciences (INSA) in Lyon, France, and has been a Debian developer since 1997. The founder of Freexian, the first French IT services company specialized in Debian GNU/Linux, he is one of the most prominent contributors to this Linux distribution."
msgstr "Ο <emphasis role=\"strong\">Raphaël Hertzog</emphasis> είναι μηχανικός υπολογιστών που αποφοίτησε από το Εθνικό Ινστιτούτο Εφαρμοσμένων Επιστημών (INSA) στη Λυών της Γαλλίας και από το 1997 εργάζεται ως δημιουργός του Debian. Ο ιδρυτής της Freexian, η πρώτη γαλλική εταιρεία παροχής υπηρεσιών πληροφορικής με εξειδίκευση στο Debian GNU/Linux, είναι ένας από τους σημαντικότερους συντελεστές αυτής της διανομής του Linux."

msgid "<emphasis role=\"strong\">Roland Mas</emphasis> is a telecommunications engineer who graduated from the National Superior School of Telecommunications (ENST) in Paris, France. A Debian developer since 2000 as well as the developer of the FusionForge software, he works as a freelance consultant who specializes in the installation and migration of Debian GNU/Linux systems and in setting up collaborative work tools."
msgstr "Ο <emphasis role=\"strong\">Roland Mas</emphasis> είναι μηχανικός τηλεπικοινωνιών που αποφοίτησε από το Εθνικό Ανώτερο Σχολείο Τηλεπικοινωνιών (ENST) στο Παρίσι της Γαλλίας. Ένας προγραμματιστής του Debian από το 2000, καθώς και ο δημιουργός του λογισμικού FusionForge, εργάζεται ως ανεξάρτητος σύμβουλος που ειδικεύεται στην εγκατάσταση και τη μετάβαση των συστημάτων Debian GNU/Linux και στη δημιουργία συνεργατικών εργαλείων εργασίας."

msgid "This book has a story. It started its life as a French-language book (Cahier de l'Admin Debian published by Eyrolles) and has been translated into English thanks to hundreds of persons who contributed to a fundraising. Learn more at https://debian-handbook.info, where you can also obtain an electronic version of this book."
msgstr "Αυτό το βιβλίο έχει μια ιστορία. Ξεκίνησε τη ζωή του ως βιβλίο γαλλικής γλώσσας (Cahier de l'Admin Debian που εκδόθηκε από την Eyrolles) και έχει μεταφραστεί στα αγγλικά χάρη σε εκατοντάδες άτομα που συνέβαλαν σε μια συγκέντρωση κεφαλαίων. Μάθετε περισσότερα στο https://debian-handbook.info, όπου μπορείτε επίσης να αποκτήσετε την ηλεκτρονική έκδοση αυτού του βιβλίου."
